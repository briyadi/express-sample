const express = require("express")
const app = express()
const port = 3000

const beforeMiddleware = (req, res, next) => {
    console.log(req.method, req.url)
    req.startedAt = new Date()
    next()
}

const afterMiddleware = (req, res) => {
    const finishedAt = new Date()
    console.log("Total execution time (ms): ", finishedAt.getTime() - req.startedAt.getTime())
    console.log("response success")
}

// app.use(beforeMiddleware)

// Before & After
app.get("/", beforeMiddleware, (req, res, next) => {
    // console.log('url', req.url)
    // console.log('method', req.method)
    // console.log('body', req.body)
    // console.log('headers', req.headers)

    res.status(200)
    // res.json({
    //     name: "Sabrina",
    //     age: 25,
    //     address: "Jakarta",
    //     wni: true
    // })
    res.sendFile(`${__dirname}/index.html`)
    next()
}, afterMiddleware)

// http://localhost:3000/products
app.get("/products", beforeMiddleware, (req, res) => {
    res.json([
        "Apple",
        "Redmi",
        "One Plus One"
    ])
})

// http://localhost:3000/orders
app.get("/orders", beforeMiddleware, (req, res) => {
    res.json([
        {
            id: 1,
            paid: false,
            user_id: 1
        },
        {
            id: 2,
            paid: false,
            user_id: 1
        }
    ])
})

// app.use(afterMiddleware)

app.listen(port, () => console.log(`express running on port ${port}`))

// http://localhost:3000